package com.c4tk.curhat.webservice;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.c4tk.curhat.model.webservice.WsRequest;

/**
 * Created by Ferico on 10/21/16.
 */

public class WsManager {
    private static WsManager sInstance;

    private RequestQueue mRequestQueue;

    public static void init(Context context) {
        if (sInstance == null) {
            sInstance = new WsManager(context);
        }
    }

    public static WsManager getInstance() {
        return sInstance;
    }

    private WsManager(Context context) {
        mRequestQueue = CurhatVolley.newRequestQueue(context);
    }

    public void addToQueue(WsRequest request) {
        mRequestQueue.add(request);
    }

    public void clear(Object tag) {
        if (tag != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public void clearAll() {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(new RequestQueue.RequestFilter() {
                @Override
                public boolean apply(Request<?> request) {
                    return true;
                }
            });
        }
    }
}
