package com.c4tk.curhat.webservice;

import com.c4tk.curhat.model.webservice.WsResponse;

/**
 * Created by Ferico on 10/21/16.
 */

public interface WsListener<T extends WsResponse> {
    void onSuccess(T response);
    void onError(String error);
}
