package com.c4tk.curhat.webservice;

import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.c4tk.curhat.model.webservice.GenderResponse;
import com.c4tk.curhat.model.webservice.HistoryResponse;
import com.c4tk.curhat.model.webservice.LocationResponse;
import com.c4tk.curhat.model.webservice.MasterResponse;
import com.c4tk.curhat.model.webservice.SuggestionResponse;
import com.c4tk.curhat.model.webservice.WsResponse;
import com.c4tk.curhat.util.Util;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Ferico on 10/20/16.
 */

public class CurhatWS {
    private static RequestQueue sRequestQueue;

    public CurhatWS(Context context) {
        if (sRequestQueue == null)
            sRequestQueue = CurhatVolley.newRequestQueue(context);
    }

    public static void clearQueue() {

        if (sRequestQueue != null) {
            sRequestQueue.cancelAll(new RequestQueue.RequestFilter() {
                @Override
                public boolean apply(Request<?> request) {
                    return true;
                }
            });
            sRequestQueue.stop();
            sRequestQueue = null;
        }
    }

    public void getChildSuggestion(int parentId, WsListener callback) {

        String url = Util.GET_CHILD_SUGGESTION_URL.replace("<suggestionId>", parentId+"");
        sRequestQueue.add(new JsonGetRequest(false, url, callback, new SuggestionResponse(), DefaultRetryPolicy.DEFAULT_MAX_RETRIES));
    }

    public void getThreadHistory(WsListener callback) {
        String url = Util.GET_THREAD_HISTORY.replace("<user_id>", 1+"");
        sRequestQueue.add(new JsonGetRequest(false, url, callback, new HistoryResponse(), DefaultRetryPolicy.DEFAULT_MAX_RETRIES));
    }

    public void getGender(WsListener callback) {
        sRequestQueue.add(new JsonGetRequest(false, Util.GET_GENDER_URL, callback, new GenderResponse(), DefaultRetryPolicy.DEFAULT_MAX_RETRIES));
    }

    public void getStatus(WsListener callback) {
        sRequestQueue.add(new JsonGetRequest(false, Util.GET_STATUS_URL, callback, new MasterResponse(), DefaultRetryPolicy.DEFAULT_MAX_RETRIES));
    }

    public void getOccupation(WsListener<MasterResponse> callback) {
        sRequestQueue.add(new JsonGetRequest(false, Util.GET_OCCUPATION_URL, callback, new MasterResponse(), DefaultRetryPolicy.DEFAULT_MAX_RETRIES));
    }
    public void getProvince(WsListener<LocationResponse> callback) {
        sRequestQueue.add(new JsonGetRequest(false, Util.GET_PROVINCE_URL, callback, new LocationResponse(), DefaultRetryPolicy.DEFAULT_MAX_RETRIES));
    }

    public void getCity(int id, WsListener<LocationResponse> callback) {
        String url = Util.GET_CITIES_URL.replace("<province_id>", id+"");
        sRequestQueue.add(new JsonGetRequest(false, url, callback, new LocationResponse(), DefaultRetryPolicy.DEFAULT_MAX_RETRIES));
    }

    public void getKecamatan(int id, WsListener<LocationResponse> callback) {
        String url = Util.GET_KECAMATAN_URL.replace("<cityId>", id+"");
        sRequestQueue.add(new JsonGetRequest(false, url, callback, new LocationResponse(), DefaultRetryPolicy.DEFAULT_MAX_RETRIES));
    }

    public void getKelurahan(int id, WsListener<LocationResponse> callback) {
        String url = Util.GET_KELURAGAN_URL.replace("<district_id>", id+"");
        sRequestQueue.add(new JsonGetRequest(false, url, callback, new LocationResponse(), DefaultRetryPolicy.DEFAULT_MAX_RETRIES));
    }

    //    User Functionality=================================================================================================================
//    public void login(String username, String password, WsListener callback) {
//
//        JSONObject wrapper = new JSONObject();
//        JSONObject requestObject = new JSONObject();
//
//        try {
//            requestObject.put("username", username);
//            requestObject.put("password", password);
//
//            wrapper.put("data", requestObject);
//            wrapper.put("apiKey", Util.API_KEY);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        //disini default passing new WsResponse(), tapi kalau ada mau dibungkus pake response model kaya yang dilakuin buat login,
//        //bisa kaya yang dibawahnya
////        sRequestQueue.add(new JsonPostRequest(false, Util.LOGIN_URL, wrapper, callback, new WsResponse(),
////                DefaultRetryPolicy.DEFAULT_MAX_RETRIES));
//
//        sRequestQueue.add(new JsonPostRequest(false, Util.LOGIN_URL, wrapper, callback, new LoginResponse(),
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES));
//    }
}
