package com.c4tk.curhat.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.c4tk.curhat.R;

/**
 * Created by Ferico on 10/22/16.
 */

public class CategoryAdapter extends BaseAdapter {

    private String[] categories = {"Keluarga", "Sekolah", "Sosial\n / Pertemanan", "Pekerjaan", "Percintaan", "Keuangan"
            , "Kekerasan\n / Kriminalitas", "Personal"};
    private int[] resources = {R.drawable.family, R.drawable.school, R.drawable.social,
            R.drawable.work, R.drawable.love, R.drawable.finance, R.drawable.bully, R.drawable.pribadi};

    private Context context;

    public CategoryAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return categories.length;
    }

    @Override
    public Object getItem(int i) {
        return categories[i];
    }

    @Override
    public long getItemId(int i) {
        return i+1;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View gridView;

        if (view == null) {
            gridView = inflater.inflate(R.layout.grid_view_category, null);
            ImageView imageView = (ImageView) gridView
                    .findViewById(R.id.imgCategory);
            TextView lblCategory = (TextView) gridView.findViewById(R.id.lblCategory);
            lblCategory.setText(categories[i]);
            imageView.setImageResource(resources[i]);
        }
        else
            gridView = view;

        return gridView;
    }
}
