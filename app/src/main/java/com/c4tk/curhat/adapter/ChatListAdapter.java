package com.c4tk.curhat.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.c4tk.curhat.ChatActivity;
import com.c4tk.curhat.R;
import com.c4tk.curhat.model.Suggestion;
import com.c4tk.curhat.util.SimpleViewHolder;

import java.util.ArrayList;

/**
 * Created by Ferico on 10/22/16.
 */

public class ChatListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;

    public ChatListAdapter(Context context) {
        this.context = context;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public int getItemCount() {
        return 8;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goToChat = new Intent(holder.itemView.getContext(), ChatActivity.class);
                holder.itemView.getContext().startActivity(goToChat);
            }
        });

        ChatListViewHolder viewHolder = (ChatListViewHolder) holder;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_list_item, parent, false);
        return new ChatListViewHolder(view);
    }

    private class ChatListViewHolder extends RecyclerView.ViewHolder {

        public ChatListViewHolder(View itemView){
            super(itemView);

        }

        public void setChat(Suggestion s) {
            Log.v("FS", s.getDescription());
        }
    }
}
