package com.c4tk.curhat.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.c4tk.curhat.R;
import com.sinch.android.rtc.messaging.Message;

import java.util.ArrayList;

/**
 * Created by yosefsamuel on 10/22/16.
 */
public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static int ITEM_TYPE_OUTGOING = 1;
    private static int ITEM_TYPE_INCOMING = 2;
    private ArrayList<Message> messages = new ArrayList<>();

    public void addMessage(Message msg) {
        this.messages.add(msg);
        Log.e("FS","ansdfausdnfuadufbay");
        notifyItemInserted(messages.size()-1);
        //notifyDataSetChanged();
    }

    public void setMessages(ArrayList<Message> messages) {
        this.messages = messages;
    }

    @Override
    public int getItemViewType(int position) {
        //ini harusnya ga hard code
        Log.e("FS SENDER ID",messages.get(position).getSenderId());
        if(messages.get(position).getSenderId().equals("user"))
            return ITEM_TYPE_OUTGOING;
        return ITEM_TYPE_INCOMING;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == ITEM_TYPE_INCOMING) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.bubble_chat_left, parent, false);
            return new ChatViewHolder(view);
        }
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.bubble_chat_right, parent, false);
        return new ChatViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final ChatViewHolder viewHolder = (ChatViewHolder) holder;
        viewHolder.setMessage(messages.get(position));
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    private class ChatViewHolder extends RecyclerView.ViewHolder {
        private TextView lblMessage;
        public ChatViewHolder(View itemView){
            super(itemView);

            lblMessage = (TextView) itemView.findViewById(R.id.chat_message);
        }

        public void setMessage(Message msg) {
            lblMessage.setText(msg.getTextBody());
        }
    }
}
