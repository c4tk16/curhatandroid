package com.c4tk.curhat.adapter;

import android.content.Context;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.c4tk.curhat.R;
import com.c4tk.curhat.fragment.CategoryFragment;
import com.c4tk.curhat.fragment.ChatListFragment;
import com.c4tk.curhat.fragment.DummyFragment;
import com.c4tk.curhat.fragment.HistoryFragment;
import com.c4tk.curhat.fragment.ProfileFragment;
import com.c4tk.curhat.fragment.SolutionFragment;
import com.c4tk.curhat.fragment.SubCategoryFragment;
import com.c4tk.curhat.model.Suggestion;

/**
 * Created by Ferico on 10/22/16.
 */

public class DashboardPagerAdapter extends FragmentStatePagerAdapter implements CategoryFragment.CategoryListener,
        SubCategoryFragment.SubCategoryListener{

    private Context context;
    private FragmentManager mFragmentManager;
    private Fragment categoryFragment;
    private int lastSelectedCategory;
    private Suggestion lastSelectedSubCategory;
    private PagerListener callback;
    public DashboardPagerAdapter(FragmentManager fm, Context context, PagerListener callback)
    {
        super(fm);
        this.mFragmentManager = fm;
        this.context = context;
        this.callback = callback;
    }

    public interface PagerListener {
        void shouldResetTabTitle();
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position)
        {
            case 0:
                if(categoryFragment == null)
                    categoryFragment = CategoryFragment.getInstance(this);
                return categoryFragment;
            case 1:
                fragment = HistoryFragment.getInstance();
                break;
            case 2:
                fragment = ChatListFragment.getInstance();
                break;
            case 3:
                fragment = ProfileFragment.getInstance();
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public void onCategorySelected(final int id) {
        lastSelectedCategory = id;

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                mFragmentManager.beginTransaction().remove(categoryFragment)
                        .commit();
                if (categoryFragment instanceof CategoryFragment){
                    categoryFragment = SubCategoryFragment.getInstance(id, DashboardPagerAdapter.this, false);
                }else{ // Instance of NextFragment
                    categoryFragment = CategoryFragment.getInstance(DashboardPagerAdapter.this);
                }
                notifyDataSetChanged();
                callback.shouldResetTabTitle();
            }
        });
    }

    @Override
    public void onSubCategorySelected(final Suggestion suggestion) {
        lastSelectedSubCategory = suggestion;
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                mFragmentManager.beginTransaction().remove(categoryFragment)
                        .commit();
                if (categoryFragment instanceof SubCategoryFragment){
                    categoryFragment = SolutionFragment.getInstance(suggestion);
                }else{
                    categoryFragment = SubCategoryFragment.getInstance(suggestion.getId(), DashboardPagerAdapter.this, true);
                }
                notifyDataSetChanged();
                callback.shouldResetTabTitle();
            }
        });
    }

    @Override
    public int getItemPosition(Object object)
    {
        //category to sub
        if (object instanceof CategoryFragment &&
                categoryFragment instanceof SubCategoryFragment) {
            return POSITION_NONE;
        }
        if (object instanceof SubCategoryFragment &&
                categoryFragment instanceof CategoryFragment) {
            return POSITION_NONE;
        }

        //sub to solution
        if (object instanceof SubCategoryFragment &&
                categoryFragment instanceof SolutionFragment) {
            return POSITION_NONE;
        }
        if (object instanceof SolutionFragment &&
                categoryFragment instanceof SubCategoryFragment) {
            return POSITION_NONE;
        }

        return POSITION_UNCHANGED;
    }

    public boolean onBackPressed() {
        if (categoryFragment instanceof SolutionFragment) {
            onSubCategorySelected(lastSelectedSubCategory);
            return true;
        } else if (categoryFragment instanceof SubCategoryFragment) {
            if(!((SubCategoryFragment)categoryFragment).onBackButtonPressed())
                onCategorySelected(lastSelectedCategory);
            return true;
        }
        return false;
    }
}
