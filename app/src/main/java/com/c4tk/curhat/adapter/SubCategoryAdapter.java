package com.c4tk.curhat.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.c4tk.curhat.R;
import com.c4tk.curhat.model.Suggestion;
import com.c4tk.curhat.util.SimpleViewHolder;

import java.util.ArrayList;

/**
 * Created by Ferico on 10/22/16.
 */

public class SubCategoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private static int ITEM_TYPE_HEADER = 1;
    private static int ITEM_TYPE_VEHICLE = 2;
    private SubCategoryAdapterListener callback;
    private ArrayList<Suggestion> suggestions;

    public interface SubCategoryAdapterListener {
        void onSubCategorySelected(int id);
    }

    public SubCategoryAdapter(Context context, ArrayList<Suggestion> suggestions, SubCategoryAdapterListener callback) {
        this.context = context;
        this.callback = callback;
        this.suggestions = suggestions;
    }

    @Override
    public long getItemId(int i) {
        return suggestions.get(i).getId();
    }

    @Override
    public int getItemCount() {
        return suggestions.size() + 1;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(position == 0)
            return;
        SubCategoryViewHolder viewHolder = (SubCategoryViewHolder) holder;
        viewHolder.setSuggestion(suggestions.get(position - 1));
    }

    @Override
    public int getItemViewType(int position) {
        if(position == 0)
            return ITEM_TYPE_HEADER;
        return ITEM_TYPE_VEHICLE;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == ITEM_TYPE_HEADER)
        {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item_sub_category_header, parent, false);
            return new SimpleViewHolder(view);
        }
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_view_item_sub_category, parent, false);

        TextView tx = (TextView)view.findViewById(R.id.lblSubCategory);
        Typeface custom_font = Typeface.createFromAsset(context.getAssets(),  "fonts/RobotoSlab-Bold.ttf");
        tx.setTypeface(custom_font);

        return new SubCategoryViewHolder(view);
    }

    private class SubCategoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView lblSubCategory;
        Suggestion suggestion;

        public SubCategoryViewHolder(View itemView){
            super(itemView);

            LinearLayout llVehicleListItem = (LinearLayout) itemView.findViewById(R.id.llSubCategory);
            llVehicleListItem.setOnClickListener(this);
            lblSubCategory = (TextView) itemView.findViewById(R.id.lblSubCategory);
        }

        public void setSuggestion(Suggestion s) {
            Log.v("FS", s.getDescription());
            suggestion = s;
            lblSubCategory.setText(s.getDescription());
        }

        @Override
        public void onClick(View v) {
            if(callback != null)
                callback.onSubCategorySelected(suggestion.getId());
        }
    }
}
