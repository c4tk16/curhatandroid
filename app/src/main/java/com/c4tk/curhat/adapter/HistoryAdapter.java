package com.c4tk.curhat.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.c4tk.curhat.R;
import com.c4tk.curhat.model.History;
import com.c4tk.curhat.model.Suggestion;
import com.c4tk.curhat.util.SimpleViewHolder;

import java.util.ArrayList;

/**
 * Created by Ferico on 10/22/16.
 */

public class HistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static int ITEM_TYPE_HEADER = 1;
    private static int ITEM_TYPE_VEHICLE = 2;
    private Context context;
    private ArrayList<History> histories;

    public HistoryAdapter(Context context, ArrayList<History> histories) {
        this.context = context;
        this.histories = histories;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(position == 0)
            return;
        HistoryViewHolder viewHolder = (HistoryViewHolder) holder;
        viewHolder.setHistory(histories.get(position - 1));
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public int getItemCount() {
        return histories.size() + 1;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == ITEM_TYPE_HEADER)
        {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item_history_header, parent, false);

            return new SimpleViewHolder(view);
        }
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_view_item_history, parent, false);

        TextView tx = (TextView)view.findViewById(R.id.lblTitle);
        Typeface custom_font = Typeface.createFromAsset(context.getAssets(),  "fonts/RobotoSlab-Bold.ttf");
        tx.setTypeface(custom_font);

        return new HistoryViewHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
        if(position == 0)
            return ITEM_TYPE_HEADER;
        return ITEM_TYPE_VEHICLE;
    }

    private class HistoryViewHolder extends RecyclerView.ViewHolder{

        private TextView lblName, lblTitle, lblDate;

        public HistoryViewHolder(View itemView){
            super(itemView);

            lblName = (TextView) itemView.findViewById(R.id.lblName);
            lblTitle = (TextView) itemView.findViewById(R.id.lblTitle);
            lblDate = (TextView) itemView.findViewById(R.id.lblDate);

            lblName.setTypeface(null, Typeface.ITALIC);
        }

        public void setHistory(History history) {
            lblName.setText(history.getUser().getName());
            lblTitle.setText(history.getDescription());
            //lblDate.setText();
        }
    }
}
