package com.c4tk.curhat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.c4tk.curhat.adapter.ChatAdapter;
import com.c4tk.curhat.util.SinchUtil;
import com.sinch.android.rtc.ClientRegistration;
import com.sinch.android.rtc.PushPair;
import com.sinch.android.rtc.SinchClient;
import com.sinch.android.rtc.SinchClientListener;
import com.sinch.android.rtc.SinchError;
import com.sinch.android.rtc.messaging.Message;
import com.sinch.android.rtc.messaging.MessageClient;
import com.sinch.android.rtc.messaging.MessageClientListener;
import com.sinch.android.rtc.messaging.MessageDeliveryInfo;
import com.sinch.android.rtc.messaging.MessageFailureInfo;
import com.sinch.android.rtc.messaging.WritableMessage;

import java.util.List;

public class ChatActivity extends AppCompatActivity implements View.OnClickListener, MessageClientListener, SinchClientListener {
    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private EditText txtChat;
    private ImageView btnSend;
    private SinchClient sinchClient;
    private MessageClient messageClient;
    private ChatAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        toolbar = (Toolbar) findViewById(R.id.chat_toolbar);
        recyclerView = (RecyclerView) findViewById(R.id.chat_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        txtChat = (EditText) findViewById(R.id.chat_text_chat);
        btnSend = (ImageView) findViewById(R.id.chat_button_send);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("James Samuel");
        getSupportActionBar().setSubtitle("online");
        getSupportActionBar().setLogo(R.drawable.orangb);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        sinchClient = SinchUtil.getInstance();
        sinchClient.addSinchClientListener(this);
        messageClient = sinchClient.getMessageClient();
        messageClient.addMessageClientListener(this);

        adapter = new ChatAdapter();
        recyclerView.setAdapter(adapter);

        btnSend.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_chat, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.chat_button_send) {
            if(txtChat.getText().length() == 0)
                return;
            WritableMessage msg = new WritableMessage("konsultan",txtChat.getText().toString());
            txtChat.setText("");
            messageClient.send(msg);
        }
    }

    @Override
    public void onIncomingMessage(MessageClient messageClient, Message message) {
        Log.e("FS INCOMING", message.getTextBody());
        adapter.addMessage(message);
    }

    @Override
    public void onMessageSent(MessageClient messageClient, Message message, String s) {
        Log.e("FS", message.getTextBody());
        adapter.addMessage(message);

        //adapter.notifyDataSetChanged();
    }

    @Override
    public void onMessageFailed(MessageClient messageClient, Message message, MessageFailureInfo messageFailureInfo) {
        Log.e("FS Message Failed", messageFailureInfo.toString());
    }

    @Override
    public void onMessageDelivered(MessageClient messageClient, MessageDeliveryInfo messageDeliveryInfo) {
        Log.e("FS Message Delivered", messageDeliveryInfo.toString());
    }

    @Override
    public void onShouldSendPushData(MessageClient messageClient, Message message, List<PushPair> list) {

    }

    @Override
    public void onClientStarted(SinchClient sinchClient) {
        Log.e("FS", "client started");
    }

    @Override
    public void onClientStopped(SinchClient sinchClient) {
        Log.e("FS", "client stopped");
    }

    @Override
    public void onClientFailed(SinchClient sinchClient, SinchError sinchError) {
        Log.e("FS", sinchError.getMessage());
    }

    @Override
    public void onRegistrationCredentialsRequired(SinchClient sinchClient, ClientRegistration clientRegistration) {

    }

    @Override
    public void onLogMessage(int i, String s, String s1) {
        Log.e("FS", s+":"+s1);
    }
}
