package com.c4tk.curhat.util;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Ferico on 10/22/16.
 */

public class SimpleViewHolder extends RecyclerView.ViewHolder {

    public SimpleViewHolder(View itemView) {
        super(itemView);
    }
}
