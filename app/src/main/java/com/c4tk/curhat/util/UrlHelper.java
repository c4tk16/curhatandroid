package com.c4tk.curhat.util;

import android.content.Context;

import com.c4tk.curhat.R;

/**
 * Created by Ferico on 10/21/16.
 */

public final class UrlHelper {

    private static UrlHelper sInstance;
    public final String baseUrl;

    public static UrlHelper getInstance() {
        return sInstance;
    }

    public static void init(Context context) {
        if (sInstance == null) {
            sInstance = new UrlHelper(context);
        }
    }

    private UrlHelper(Context context) {
        baseUrl = context.getString(R.string.host);
    }

    public String getUrl(String path) {
        return baseUrl + path;
    }
}