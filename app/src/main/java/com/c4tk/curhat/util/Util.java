package com.c4tk.curhat.util;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.HandlerThread;

import com.c4tk.curhat.MainActivity;
import com.c4tk.curhat.R;
import com.c4tk.curhat.webservice.CurhatWS;
import com.c4tk.curhat.webservice.WsManager;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Ferico on 10/21/16.
 */

public class Util {

    private static Context context;
    private static Handler saverHandler;
    private static HandlerThread handlerThread;

    public static final String SHARED_PREFERENCES_NAME = "com.c4tk.curhat.SharedPreferences";
    public static final int SPLASH_SCREEN_DURATION = 2000;

    public static final String SINCH_APPLICATION_KEY = "d7d8ff1a-83fb-4be5-bba0-29ce04a720fd";
    public static final String SINCH_APPLICATION_SECRET = "HJURdANX6EKWukFHsOcJcw==";
    public static final String SINCH_HOSTNAME = "sandbox.sinch.com";

    private static final String PREFIX = "com.c4tk";
    public static final String KEY_USER_ID = PREFIX + "userID";
    public static final String KEY_ACCESS_TOKEN = PREFIX + "accessToken";
    public static final String KEY_USER_NAME = PREFIX + "name";

    public static final String API_KEY = "";
    private static final String BASE_URL = "http://tersebut.com/api/v1/";
    public static final String LOGIN_URL = BASE_URL + "user/login";
    public static final String GET_CHILD_SUGGESTION_URL = BASE_URL + "suggestions/<suggestionId>";
    public static final String GET_THREAD_HISTORY = BASE_URL + "users/<user_id>/threads";
    public static final String GET_GENDER_URL = BASE_URL + "genders";
    public static final String GET_STATUS_URL = BASE_URL + "marital-statuses";
    public static final String GET_OCCUPATION_URL = BASE_URL + "occupations";
    public static final String GET_PROVINCE_URL = BASE_URL + "provinces";
    public static final String GET_CITIES_URL = BASE_URL + "provinces/<province_id>/cities";
    public static final String GET_KECAMATAN_URL = BASE_URL + "cities/<cityId>/districts";
    public static final String GET_KELURAGAN_URL = BASE_URL + "districts/<district_id>/villages";

    static {
        handlerThread = new HandlerThread("SaverHandlerThread");
        handlerThread.start();
        saverHandler = new Handler(handlerThread.getLooper());
    }

    public static void setContext(Context context) {
        // Get Application Context instead of Activity Context to prevent activity leak
        if (Util.context == null)
            Util.context = context.getApplicationContext();
    }

    public static HandlerThread getHandlerThread() {
        return handlerThread;
    }

    public static void SET_USER_ID(String userID) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        sharedPreferences.edit().putString(KEY_USER_ID, userID).apply();
    }

    public static String GET_USER_ID() {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_USER_ID, "");
    }

    public static void SET_ACCESS_TOKEN(String userID) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        sharedPreferences.edit().putString(KEY_ACCESS_TOKEN, userID).apply();
    }

    public static String GET_ACCESS_TOKEN() {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_ACCESS_TOKEN, "");
    }

    public static String GET_USER_NAME() {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_USER_NAME, "");
    }

    public static void SET_USER_NAME(String name) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        sharedPreferences.edit().putString(KEY_USER_NAME, name).apply();
    }

    public static void logOut() {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        sharedPreferences.edit().clear().apply();
        CurhatWS.clearQueue();
        WsManager.getInstance().clearAll();
    }

    public static void forceLogOut() {
        logOut();
        Intent reloginIntent = new Intent(context, MainActivity.class);
        reloginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(reloginIntent);
    }

    public static String CountDuration(String inTime,String outTime) throws ParseException {

        SimpleDateFormat simpleDateFormat =
                new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date startDate = simpleDateFormat.parse(inTime);
        Date endDate = simpleDateFormat.parse(outTime);
        long different = endDate.getTime() - startDate.getTime();


        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        String duration = "";
        if(elapsedDays == 0){
            duration += "";
        }else{
            duration += String.valueOf(elapsedDays) + "days ";
        }

        if(elapsedHours == 0){
            duration += "";
        }else{
            duration += String.valueOf(elapsedHours) + "hours ";
        }

        if(elapsedMinutes == 0){
            duration += "";
        }else{
            duration += String.valueOf(elapsedMinutes) + "mins ";
        }

        if(elapsedSeconds == 0){
            duration += "";
        }else{
            duration += String.valueOf(elapsedSeconds) + "secs ";
        }
        return duration;
    }
}