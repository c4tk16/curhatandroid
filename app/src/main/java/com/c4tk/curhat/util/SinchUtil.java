package com.c4tk.curhat.util;

import android.content.Context;

import com.sinch.android.rtc.Sinch;
import com.sinch.android.rtc.SinchClient;
import com.sinch.android.rtc.SinchClientListener;

/**
 * Created by Ferico on 10/22/16.
 */

public class SinchUtil {
    private static SinchClient instance;
    private static Context context;

    public static void setContext(Context context) {
        SinchUtil.context = context;
    }

    public static SinchClient getInstance() {
        if(instance == null) {
            instance = Sinch.getSinchClientBuilder().context(SinchUtil.context)
                    .applicationKey(Util.SINCH_APPLICATION_KEY)
                    .applicationSecret(Util.SINCH_APPLICATION_SECRET)
                    .environmentHost(Util.SINCH_HOSTNAME)
                    .userId("user123")
                    .build();

            instance.setSupportMessaging(true);
            instance.setSupportCalling(true);
            instance.setSupportActiveConnectionInBackground(true);
            instance.start();
        }
        return instance;
    }

    public static void setListener(SinchClientListener listener) {
        instance.addSinchClientListener(listener);
    }

    public static void removeListener(SinchClientListener listener) {
        instance.removeSinchClientListener(listener);
    }

    public static void stopClient() {
        instance.stopListeningOnActiveConnection();
        instance.terminate();
        instance = null;
    }
}
