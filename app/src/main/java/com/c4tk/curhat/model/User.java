package com.c4tk.curhat.model;

/**
 * Created by Ferico on 10/21/16.
 */

public class User {
    private int userId;
    private String sinchId;
    private String name;
    private String nickName;
    private String email;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getSinchId() {
        return sinchId;
    }

    public void setSinchId(String sinchId) {
        this.sinchId = sinchId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
