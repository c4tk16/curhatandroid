package com.c4tk.curhat.model.webservice;

import android.util.Log;

import com.c4tk.curhat.model.Counselor;
import com.c4tk.curhat.model.History;
import com.c4tk.curhat.model.Suggestion;
import com.c4tk.curhat.model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Ferico on 10/22/16.
 */

public class HistoryResponse extends WsResponse {

    private User user;
    private int historyId;
    private String description;
    private String category;
    private String consultationType;
    private String consultationTypeDescription;
    private Counselor counselor;

    public ArrayList<History> getHistories() {
        return histories;
    }

    private ArrayList<History> histories;

    @Override
    protected void parseContent(JSONObject jsonObject) {
        try {
            super.parseContent(jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        user = new User();
        counselor = new Counselor();
        try {

            histories = new ArrayList<>();
            JSONObject bigData = jsonObject.getJSONObject("data");


            JSONArray dataArray = bigData.getJSONArray("data");

            for(int i=0; i<dataArray.length(); i++) {
                JSONObject data = dataArray.getJSONObject(i);

                historyId = data.getInt("id");
                description = data.getString("description");

                JSONObject catObj = data.getJSONObject("category");
                category = catObj.getString("description");

                JSONObject conObj = data.getJSONObject("consultation_type");
                consultationType = conObj.getString("name");
                consultationTypeDescription = conObj.getString("description");

                JSONObject uObj = data.getJSONObject("user");
                user.setUserId(uObj.getInt("id"));
                user.setSinchId(uObj.getString("sinch_id"));
                user.setEmail(uObj.getString("email"));
                user.setNickName(uObj.getString("nick_name"));
                user.setName(uObj.getString("name"));

                JSONObject cObj = data.getJSONObject("counselor");
                counselor.setId(cObj.getInt("id"));
                counselor.setBirthPlace(cObj.getString("birth_place"));
                counselor.setEmail(cObj.getString("email"));
                counselor.setNickName(cObj.getString("nick_name"));
                counselor.setName(cObj.getString("name"));
                counselor.setAddress(cObj.getString("address"));
                counselor.setSinchId(cObj.getString("sinch_id"));

                History history = new History();
                history.setCategory(category);
                history.setConsultationType(consultationType);
                history.setConsultationTypeDescription(consultationTypeDescription);
                history.setCounselor(counselor);
                history.setHistoryId(historyId);
                history.setUser(user);
                history.setDescription(description);

                histories.add(history);
            }
        }
        catch (JSONException e)
        {
            Log.v("FS EXCEPTION", e.getMessage());
            e.printStackTrace();
        }
    }
}
