package com.c4tk.curhat.model;

/**
 * Created by Ferico on 10/22/16.
 */

public class History {
    private User user;
    private int historyId;
    private String description;
    private String category;
    private String consultationType;
    private String consultationTypeDescription;
    private Counselor counselor;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getHistoryId() {
        return historyId;
    }

    public void setHistoryId(int historyId) {
        this.historyId = historyId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getConsultationType() {
        return consultationType;
    }

    public void setConsultationType(String consultationType) {
        this.consultationType = consultationType;
    }

    public String getConsultationTypeDescription() {
        return consultationTypeDescription;
    }

    public void setConsultationTypeDescription(String consultationTypeDescription) {
        this.consultationTypeDescription = consultationTypeDescription;
    }

    public Counselor getCounselor() {
        return counselor;
    }

    public void setCounselor(Counselor counselor) {
        this.counselor = counselor;
    }
}
