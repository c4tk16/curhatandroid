package com.c4tk.curhat.model.webservice;

import android.util.Log;

import com.c4tk.curhat.model.Counselor;
import com.c4tk.curhat.model.Suggestion;
import com.c4tk.curhat.model.User;
import com.rebtel.repackaged.com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Ferico on 10/22/16.
 */

public class SuggestionResponse extends WsResponse {
    private Suggestion suggestion;

    public Suggestion getSuggestion() {
        return suggestion;
    }

    public void setSuggestion(Suggestion suggestion) {
        this.suggestion = suggestion;
    }

    @Override
    protected void parseContent(JSONObject jsonObject) {
        try {
            super.parseContent(jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        suggestion = new Suggestion();
        try {

            JSONArray dataArray = jsonObject.getJSONArray("data");
            JSONObject data = dataArray.getJSONObject(0);

            suggestion.setId(data.getInt("id"));
            suggestion.setDescription(data.getString("description"));
            suggestion.setUrgentRating(data.getInt("urgent_rating"));

            if(!data.isNull("parent")) {
                JSONObject parentData = data.getJSONObject("parent");
                Suggestion parent = new Suggestion();
                parent.setId(parentData.getInt("id"));
                parent.setDescription(parentData.getString("description"));
                parent.setUrgentRating(parentData.getInt("urgent_rating"));
                suggestion.setParent(parent);
            }

            ArrayList<Suggestion> childs = new ArrayList<>();
            JSONArray children = data.getJSONArray("childs");
            for(int i=0; i<children.length(); i++)
            {
                JSONObject childObject = children.getJSONObject(i);
                Suggestion child = new Suggestion();
                child.setId(childObject.getInt("id"));
                child.setDescription(childObject.getString("description"));
                child.setUrgentRating(childObject.getInt("urgent_rating"));
                childs.add(child);
                Log.v("FS",child.getDescription());
            }
            suggestion.setChilds(childs);

            ArrayList<Counselor> cs = new ArrayList<>();
            JSONArray counselors = data.getJSONArray("councelors");
            for(int i=0; i<counselors.length(); i++) {
                JSONObject co = counselors.getJSONObject(i);
                Counselor c = new Counselor();
                c.setId(co.getInt("id"));
                c.setName(co.getString("name"));
                c.setNickName(co.getString("nick_name"));
                c.setEmail(co.getString("email"));
                c.setProfile(co.getString("profile"));
                c.setBirthDate(co.getString("birth_date"));
                c.setBirthPlace(co.getString("birth_place"));
                c.setAddress(co.getString("birth_date"));
                cs.add(c);
            }
            suggestion.setCounselors(cs);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }
}
