package com.c4tk.curhat.model;

import java.util.ArrayList;

/**
 * Created by Ferico on 10/22/16.
 */

public class Suggestion {
    private int id;
    private String description;
    private int urgentRating;
    private Suggestion parent;
    private ArrayList<Suggestion> childs;
    private ArrayList<Counselor> counselors;

    public ArrayList<Counselor> getCounselors() {
        return counselors;
    }

    public void setCounselors(ArrayList<Counselor> counselors) {
        this.counselors = counselors;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getUrgentRating() {
        return urgentRating;
    }

    public void setUrgentRating(int urgentRating) {
        this.urgentRating = urgentRating;
    }

    public Suggestion getParent() {
        return parent;
    }

    public void setParent(Suggestion parent) {
        this.parent = parent;
    }

    public ArrayList<Suggestion> getChilds() {
        return childs;
    }

    public void setChilds(ArrayList<Suggestion> childs) {
        this.childs = childs;
    }
}
