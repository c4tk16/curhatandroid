package com.c4tk.curhat.model.webservice;

import android.util.Log;

import com.c4tk.curhat.model.Counselor;
import com.c4tk.curhat.model.Suggestion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Ferico on 10/22/16.
 */

public class GenderResponse extends WsResponse {
    public ArrayList<String> getGenders() {
        return genders;
    }

    private ArrayList<String> genders;

    @Override
    protected void parseContent(JSONObject jsonObject) {
        try {
            super.parseContent(jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            JSONArray dataArray = jsonObject.getJSONArray("data");
            genders = new ArrayList<>();
            Log.e("FS", jsonObject.toString());
            for(int i=0; i<dataArray.length(); i++) {
                JSONObject data = dataArray.getJSONObject(i);
                String description = data.getString("description");
                Log.e("FS EXCEPTION", description);
                genders.add(description);
            }

        }
        catch (JSONException e)
        {
            Log.e("FS EXCEPTION", e.getMessage());
            e.printStackTrace();
        }
    }
}
