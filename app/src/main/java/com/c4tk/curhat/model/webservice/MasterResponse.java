package com.c4tk.curhat.model.webservice;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Ferico on 10/22/16.
 */

public class MasterResponse extends WsResponse {

    public ArrayList<String> getResults() {
        return results;
    }

    private ArrayList<String> results;

    @Override
    protected void parseContent(JSONObject jsonObject) {
        try {
            super.parseContent(jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            JSONArray dataArray = jsonObject.getJSONArray("data");
            results = new ArrayList<>();
            Log.e("FS", jsonObject.toString());
            for(int i=0; i<dataArray.length(); i++) {
                JSONObject data = dataArray.getJSONObject(i);
                String description = data.getString("description");
                results.add(description);
            }

        }
        catch (JSONException e)
        {
            Log.e("FS EXCEPTION", e.getMessage());
            e.printStackTrace();
        }
    }
}
