package com.c4tk.curhat;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.c4tk.curhat.util.SinchUtil;
import com.c4tk.curhat.util.Util;
import com.c4tk.curhat.webservice.WsManager;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);

        Util.setContext(this);
        SinchUtil.setContext(this);
        WsManager.init(this);

        ImageView imgSplashScreen = (ImageView) findViewById(R.id.imgSplashScreen);
        imgSplashScreen.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashScreenActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }, Util.SPLASH_SCREEN_DURATION);
    }
}
