package com.c4tk.curhat;

import android.graphics.Color;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;

import com.c4tk.curhat.adapter.DashboardPagerAdapter;
import com.c4tk.curhat.adapter.DashboardPagerAdapter.PagerListener;
import com.c4tk.curhat.util.SinchUtil;

public class MainActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener, PagerListener {

    DashboardPagerAdapter adapter;
    ViewPager mViewPager;
    private TabLayout mTabLayout;
    private AppBarLayout mAppBarLayout;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.parseColor("#65C6D4"));
        toolbar.setVisibility(View.GONE);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        adapter = new DashboardPagerAdapter(getSupportFragmentManager(), MainActivity.this, this);
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setOnPageChangeListener(this);
        mViewPager.setAdapter(adapter);
        mViewPager.setOffscreenPageLimit(3);
        mAppBarLayout = (AppBarLayout) findViewById(R.id.appbar);

        mTabLayout = (TabLayout) findViewById(R.id.tabs);
        mTabLayout.setupWithViewPager(mViewPager);

        SinchUtil.getInstance();

        setupTabLayout(0);
    }

    private void setupTabLayout(int  position) {
        mTabLayout.getTabAt(0).setIcon(R.drawable.rumah);
        mTabLayout.getTabAt(1).setIcon(R.drawable.history);
        mTabLayout.getTabAt(2).setIcon(R.drawable.chat);
        mTabLayout.getTabAt(3).setIcon(R.drawable.orang);

        switch (position){
            case 0:   mTabLayout.getTabAt(0).setIcon(R.drawable.rumahb);break;
            case 1:   mTabLayout.getTabAt(1).setIcon(R.drawable.historyb);break;
            case 2:   mTabLayout.getTabAt(2).setIcon(R.drawable.chatb);break;
            case 3:   mTabLayout.getTabAt(3).setIcon(R.drawable.orangb);break;
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onBackPressed() {
        if(!adapter.onBackPressed())
            super.onBackPressed();
    }

    @Override
    public void onPageSelected(int position) {
        setupTabLayout(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void shouldResetTabTitle() {
        setupTabLayout(0);
    }
}
