package com.c4tk.curhat.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.c4tk.curhat.R;
import com.c4tk.curhat.adapter.CategoryAdapter;
import com.c4tk.curhat.adapter.SubCategoryAdapter;
import com.c4tk.curhat.adapter.SubCategoryAdapter.SubCategoryAdapterListener;
import com.c4tk.curhat.model.Suggestion;
import com.c4tk.curhat.model.webservice.SuggestionResponse;
import com.c4tk.curhat.webservice.CurhatWS;
import com.c4tk.curhat.webservice.WsListener;

import java.util.ArrayList;

/**
 * Created by Ferico on 10/22/16.
 */

public class SubCategoryFragment extends Fragment implements SubCategoryAdapterListener, WsListener<SuggestionResponse> {
    private static SubCategoryFragment instance;
    private SubCategoryListener callback;
    private RecyclerView mRecyclerView;
    private ArrayList<Integer> requestedIds;
    private int subCategoryId = 0;
    private boolean shouldLoad = false;
    private ProgressDialog mProgressDialog;
    private SubCategoryAdapter adapter;
    private boolean isNavigatingBack = false;

    public static SubCategoryFragment getInstance(int id, SubCategoryListener callback, boolean isNavigatingBack) {
        if(instance == null)
            instance = new SubCategoryFragment(callback);
        if(id != instance.subCategoryId) {
            instance.subCategoryId = id;
            instance.shouldLoad = true;
        }
        instance.isNavigatingBack = isNavigatingBack;
        return instance;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(shouldLoad && !isNavigatingBack) {
            Log.v("FS","atas");
            notifyDataSetChanged();
        }
        else if(isNavigatingBack) {
            if(requestedIds.size() - 1 < 0)
            {
                callback.onSubCategorySelected(null);
                return;
            }
            subCategoryId = requestedIds.get(requestedIds.size() - 1);
            requestedIds.remove(requestedIds.size() - 1);
            Log.v("FS","bawah: "+subCategoryId);
            notifyDataSetChanged();
        }
    }

    private void notifyDataSetChanged() {
        CurhatWS ws = new CurhatWS(getContext());
        mProgressDialog = ProgressDialog.show(getContext(), getString(R.string.loading),
                getString(R.string.please_wait), true, false);
        ws.getChildSuggestion(subCategoryId, this);

    }

    private SubCategoryFragment(SubCategoryListener callback) {
        this.callback = callback;
        requestedIds = new ArrayList<>();
    }

    @Override
    public void onSubCategorySelected(int id) {
        this.subCategoryId = id;
        isNavigatingBack = false;
        notifyDataSetChanged();
        //cek ada child lagi atau kaga
        //kalau ada push ke array list
        // kalau ga ada panggil callback biar pindah halaman
        //callback.onSubCategorySelected(id);
    }

    public boolean onBackButtonPressed() {
        if(requestedIds.size() - 1 > 0) {
            requestedIds.remove(requestedIds.size() - 1);
            subCategoryId = requestedIds.get(requestedIds.size() - 1);
            isNavigatingBack = true;
            notifyDataSetChanged();
            return true;
        }
        return false;
    }

    @Override
    public void onSuccess(SuggestionResponse response) {
        mProgressDialog.dismiss();
        if(response.getSuggestion().getChilds() == null || response.getSuggestion().getChilds().size() == 0 ||
                response.getSuggestion().getChilds().get(0).getDescription().contains("<p>"))
        {
            callback.onSubCategorySelected(response.getSuggestion().getChilds().get(0));
            return;
        }
        if(!isNavigatingBack)
            requestedIds.add(subCategoryId);
        adapter = new SubCategoryAdapter(getContext(), response.getSuggestion().getChilds(), this);
        mRecyclerView.setAdapter(adapter);
    }

    @Override
    public void onError(String error) {
        mProgressDialog.dismiss();
    }

    public interface SubCategoryListener {
        void onSubCategorySelected(Suggestion suggestion);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sub_category, container, false);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        return rootView;
    }
}
