package com.c4tk.curhat.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.c4tk.curhat.R;
import com.c4tk.curhat.adapter.CategoryAdapter;

/**
 * Created by Ferico on 10/22/16.
 */

public class CategoryFragment extends Fragment {
    private static CategoryFragment instance;
    private CategoryListener callback;

    public static CategoryFragment getInstance(CategoryListener callback) {
        if(instance == null)
            instance = new CategoryFragment(callback);
        return instance;
    }

    private CategoryFragment(CategoryListener callback) {
        this.callback = callback;
    }

    public interface CategoryListener {
        void onCategorySelected(int id);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_category, container, false);

        GridView gridView = (GridView) rootView.findViewById(R.id.gridViewCategory);
        CategoryAdapter adapter = new CategoryAdapter(getContext());
        gridView.setAdapter(adapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                callback.onCategorySelected(i+1);
                //navigate to other activity
                //Intent intent = new Intent(CategoryActivity.this, MainActivity.class);
            }
        });
        return rootView;
    }
}
