package com.c4tk.curhat.fragment;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;

import com.c4tk.curhat.R;
import com.c4tk.curhat.adapter.HistoryAdapter;
import com.c4tk.curhat.adapter.SubCategoryAdapter;
import com.c4tk.curhat.model.webservice.HistoryResponse;
import com.c4tk.curhat.webservice.CurhatWS;
import com.c4tk.curhat.webservice.WsListener;

/**
 * Created by Ferico on 10/22/16.
 */

public class HistoryFragment extends Fragment implements WsListener<HistoryResponse> {
    private static HistoryFragment instance;
    private RecyclerView mRecyclerView;
    private ProgressDialog mProgressDialog;

    public static HistoryFragment getInstance() {
        if(instance == null)
            instance = new HistoryFragment();
        return instance;
    }

    @Override
    public void onResume() {
        super.onResume();
        CurhatWS ws = new CurhatWS(getContext());
        mProgressDialog = ProgressDialog.show(getContext(), getString(R.string.loading),
                getString(R.string.please_wait), true, false);
        ws.getThreadHistory(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_history, container, false);


        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        //HistoryAdapter adapter = new HistoryAdapter(getContext());
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        //mRecyclerView.setAdapter(adapter);

        return rootView;
    }

    @Override
    public void onSuccess(HistoryResponse response) {
        mProgressDialog.dismiss();

        HistoryAdapter adapter = new HistoryAdapter(getContext(), response.getHistories());
        mRecyclerView.setAdapter(adapter);
    }

    @Override
    public void onError(String error) {

    }
}
