package com.c4tk.curhat.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.c4tk.curhat.R;
import com.c4tk.curhat.model.Location;
import com.c4tk.curhat.model.webservice.GenderResponse;
import com.c4tk.curhat.model.webservice.LocationResponse;
import com.c4tk.curhat.model.webservice.MasterResponse;
import com.c4tk.curhat.model.webservice.WsResponse;
import com.c4tk.curhat.webservice.CurhatWS;
import com.c4tk.curhat.webservice.WsListener;

import java.util.ArrayList;

/**
 * Created by Ferico on 10/22/16.
 */

public class ProfileFragment extends Fragment {
    private static ProfileFragment instance;
    private ArrayList<Location> provinces, cities, kecamatans, keluarahans;
    Spinner spinnerGender, spinnerStatus, spinnerPekerjaan, spinnerKota, spinnerKelurahan, spinnerProvince, spinnerKecamatan;

    public static ProfileFragment getInstance() {
        if(instance == null)
            instance = new ProfileFragment();
        return instance;
    }

    @Override
    public void onResume() {
        super.onResume();

        CurhatWS ws = new CurhatWS(getContext());
        ws.getGender(new WsListener<GenderResponse>() {
            @Override
            public void onSuccess(GenderResponse response) {
                String[] genders = new String[response.getGenders().size() + 1];
                ArrayList<String> gender = response.getGenders();
                genders[0] = "Jenis Kelamin";
                for(int i=1; i<genders.length; i++) {
                    genders[i] = gender.get(i-1);
                }
                ArrayAdapter<String> adapter = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, genders);
                adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
                spinnerGender.setAdapter(adapter);
            }

            @Override
            public void onError(String error) {

            }
        });

        ws.getStatus(new WsListener<MasterResponse>() {
            @Override
            public void onSuccess(MasterResponse response) {
                String[] genders = new String[response.getResults().size() + 1];
                ArrayList<String> gender = response.getResults();
                genders[0] = "Status";
                for(int i=1; i<genders.length; i++) {
                    genders[i] = gender.get(i-1);
                }
                ArrayAdapter<String> adapter = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, genders);
                adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
                spinnerStatus.setAdapter(adapter);
            }

            @Override
            public void onError(String error) {

            }
        });

        ws.getOccupation(new WsListener<MasterResponse>() {
            @Override
            public void onSuccess(MasterResponse response) {
                String[] genders = new String[response.getResults().size() + 1];
                ArrayList<String> gender = response.getResults();
                genders[0] = "Pekerjaan";
                for(int i=1; i<genders.length; i++) {
                    genders[i] = gender.get(i-1);
                }
                ArrayAdapter<String> adapter = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, genders);
                adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
                spinnerPekerjaan.setAdapter(adapter);
            }

            @Override
            public void onError(String error) {

            }
        });

        final AdapterView.OnItemSelectedListener kecamatanSelected = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                int id = kecamatans.get(i).getId();
                CurhatWS ws2 = new CurhatWS(getContext());
                ws2.getKelurahan(id, new WsListener<LocationResponse>() {
                    @Override
                    public void onSuccess(LocationResponse response) {
                        String[] genders = new String[response.getResults().size() + 1];
                        ArrayList<String> gender = response.getResults();
                        genders[0] = "Kelurahan";
                        for(int i=1; i<genders.length; i++) {
                            genders[i] = gender.get(i-1);
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, genders);
                        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
                        spinnerKelurahan.setAdapter(adapter);
                        keluarahans = response.getLocations();
                    }

                    @Override
                    public void onError(String error) {

                    }
                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        };

        final AdapterView.OnItemSelectedListener citySelected = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                int id = cities.get(i).getId();
                CurhatWS ws2 = new CurhatWS(getContext());
                ws2.getKecamatan(id, new WsListener<LocationResponse>() {
                    @Override
                    public void onSuccess(LocationResponse response) {
                        String[] genders = new String[response.getResults().size() + 1];
                        ArrayList<String> gender = response.getResults();
                        genders[0] = "Kecamatan";
                        for(int i=1; i<genders.length; i++) {
                            genders[i] = gender.get(i-1);
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, genders);
                        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
                        spinnerKecamatan.setAdapter(adapter);
                        spinnerKecamatan.setOnItemSelectedListener(kecamatanSelected);
                        kecamatans = response.getLocations();
                    }

                    @Override
                    public void onError(String error) {

                    }
                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        };

        final AdapterView.OnItemSelectedListener provinceSelected = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                int id = provinces.get(i).getId();
                CurhatWS ws2 = new CurhatWS(getContext());
                ws2.getCity(id, new WsListener<LocationResponse>() {
                    @Override
                    public void onSuccess(LocationResponse response) {
                        String[] genders = new String[response.getResults().size() + 1];
                        ArrayList<String> gender = response.getResults();
                        genders[0] = "Kota";
                        for(int i=1; i<genders.length; i++) {
                            genders[i] = gender.get(i-1);
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, genders);
                        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
                        spinnerKota.setAdapter(adapter);
                        spinnerKota.setOnItemSelectedListener(citySelected);
                        cities = response.getLocations();
                    }

                    @Override
                    public void onError(String error) {

                    }
                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        };

        ws.getProvince(new WsListener<LocationResponse>() {
            @Override
            public void onSuccess(LocationResponse response) {
                String[] genders = new String[response.getResults().size() + 1];
                ArrayList<String> gender = response.getResults();
                genders[0] = "Provinsi";
                for(int i=1; i<genders.length; i++) {
                    genders[i] = gender.get(i-1);
                }
                ArrayAdapter<String> adapter = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, genders);
                adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
                spinnerProvince.setAdapter(adapter);
                spinnerProvince.setOnItemSelectedListener(provinceSelected);
                provinces = response.getLocations();
            }

            @Override
            public void onError(String error) {

            }
        });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);

        spinnerGender = (Spinner) rootView.findViewById(R.id.spinnerJenisKelamin);
        spinnerStatus = (Spinner) rootView.findViewById(R.id.spinnerStatus);
        spinnerKelurahan = (Spinner) rootView.findViewById(R.id.spinnerKelurahan);
        spinnerKota = (Spinner) rootView.findViewById(R.id.spinnerKota);
        spinnerPekerjaan = (Spinner) rootView.findViewById(R.id.spinnerPekerjaan);
        spinnerProvince = (Spinner) rootView.findViewById(R.id.spinnerProvince);
        spinnerKecamatan = (Spinner) rootView.findViewById(R.id.spinnerKecamatan);

        return rootView;
    }
}
