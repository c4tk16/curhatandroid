package com.c4tk.curhat.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.c4tk.curhat.ChatActivity;
import com.c4tk.curhat.R;
import com.c4tk.curhat.adapter.HistoryAdapter;
import com.c4tk.curhat.model.Suggestion;

/**
 * Created by Ferico on 10/22/16.
 */

public class SolutionFragment extends Fragment {
    private static SolutionFragment instance;
    private Suggestion suggestion;
    private TextView lblSolution;

    public static SolutionFragment getInstance(Suggestion suggestion) {
        if(instance == null)
            instance = new SolutionFragment();
        instance.suggestion = suggestion;
        return instance;
    }

    @Override
    public void onResume() {
        super.onResume();

        lblSolution.setText(Html.fromHtml(suggestion.getDescription()));
        //lblSolution.set
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_solution, container, false);

        lblSolution = (TextView) rootView.findViewById(R.id.lblSolusi);

        Button btnNo = (Button) rootView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setPositiveButton(R.string.chat, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(getContext(), ChatActivity.class);
                        getContext().startActivity(intent);
                    }
                });

                builder.setMessage(R.string.negative_msg);

                AlertDialog dialog = builder.create();

                dialog.show();

            }
        });

        Button btnYes = (Button) rootView.findViewById(R.id.btnYes);
        btnYes.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setPositiveButton(R.string.chat, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(getContext(), ChatActivity.class);
                        getContext().startActivity(intent);
                    }
                });

                builder.setMessage(R.string.dialog_message);

                AlertDialog dialog = builder.create();

                dialog.show();

            }
        });

        return rootView;
    }
}
