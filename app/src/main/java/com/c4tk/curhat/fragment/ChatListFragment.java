package com.c4tk.curhat.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.c4tk.curhat.R;
import com.c4tk.curhat.adapter.ChatListAdapter;

/**
 * Created by Ferico on 10/22/16.
 */

public class ChatListFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private static ChatListFragment instance;
    public static ChatListFragment getInstance() {
        if(instance == null)
            instance = new ChatListFragment();
        return instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_chat_list, container, false);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        ChatListAdapter adapter = new ChatListAdapter(getContext());
        //HistoryAdapter adapter = new HistoryAdapter(getContext());
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setAdapter(adapter);

        return rootView;
    }
}
