package com.c4tk.curhat.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.c4tk.curhat.R;
import com.c4tk.curhat.adapter.CategoryAdapter;

/**
 * Created by Ferico on 10/22/16.
 */

public class DummyFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_solution, container, false);

        return rootView;
    }
}
